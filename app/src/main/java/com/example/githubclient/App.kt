package com.example.githubclient

import androidx.multidex.MultiDexApplication
import com.example.githubclient.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.KoinContextHandler
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        KoinContextHandler.stop()
        startKoin {
            if (BuildConfig.DEBUG) androidLogger(Level.ERROR)
            androidContext(this@App)
            modules(
                NetworkModule.createModule(),
                dataModule,
                viewModelModule,
                interactors
            )
        }
    }
}