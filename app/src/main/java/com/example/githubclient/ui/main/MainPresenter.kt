package com.example.githubclient.ui.main

import com.example.githubclient.common.BasePresenter
import com.example.githubclient.domain.RepositoryInteractor
import com.example.githubclient.ui.model.UIRepositoryItem
import kotlinx.coroutines.launch

class MainPresenter(
    private val repositoryInteractor: RepositoryInteractor
) :
    BasePresenter<MainContract.View>(),
    MainContract.Presenter {

    private var prevAttachedView: MainContract.View? = null
    private var isViewReattached = false

    override fun attach(view: MainContract.View) {
        super.attach(view)
        isViewReattached = view == prevAttachedView
        prevAttachedView = view
    }

    override fun searchRepositories(query: String) {
        launch {
            view?.showLoader()
            try {
                val listRepositories = repositoryInteractor.searchRepository(query)
                view?.setRepositories(listRepositories)
                view?.hideLoader()
            } catch (e: Exception) {
                view?.hideLoader()
                e.printStackTrace()
            }
        }
    }

    override fun saveRepository(repository: UIRepositoryItem) {
        launch {
            repositoryInteractor.saveRepository(repository)
        }
    }

}