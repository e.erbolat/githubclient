package com.example.githubclient.ui.main

import com.example.githubclient.common.MvpPresenter
import com.example.githubclient.common.MvpView
import com.example.githubclient.ui.model.UIRepositoryItem

interface MainContract {

    interface View : MvpView {
        fun setRepositories(items: List<UIRepositoryItem>)
        fun showLoader()
        fun hideLoader()
    }

    interface Presenter : MvpPresenter<View> {
        fun searchRepositories(text: String)
        fun saveRepository(repository: UIRepositoryItem)
    }
}
