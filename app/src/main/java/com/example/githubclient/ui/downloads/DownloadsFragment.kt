package com.example.githubclient.ui.downloads

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.drakeet.multitype.MultiTypeAdapter
import com.example.githubclient.R
import com.example.githubclient.common.presenter
import com.example.githubclient.ui.main.viewBinder.RepositoryItemViewBinder
import com.example.githubclient.ui.model.UIRepositoryItem
import kotlinx.android.synthetic.main.fragment_downloads.*
import kotlinx.android.synthetic.main.fragment_main.recyclerView


class DownloadsFragment : Fragment(R.layout.fragment_downloads), DownloadsContract.View {
    private val multiTypeAdapter = MultiTypeAdapter()

    private val presenter: DownloadsPresenter by presenter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity?)?.setSupportActionBar(toolbar)
        initAdapter()
        presenter.getRepositories()
    }

    private fun initAdapter() {
        multiTypeAdapter.apply {
            register(RepositoryItemViewBinder(false, {
            }, {
            }))
        }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = multiTypeAdapter
        }
    }

    override fun setRepositories(items: List<UIRepositoryItem>) {
        multiTypeAdapter.items = items
        multiTypeAdapter.notifyDataSetChanged()
    }
}