package com.example.githubclient.ui.model

data class UIRepositoryItem(
    val id: Int,
    val name: String,
    val owner: UIOwner,
    val url: String,
    val downloadUrl: String,
    var downloaded: Int
)

data class UIOwner(
    val id: String,
    val name: String
)