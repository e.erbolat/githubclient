package com.example.githubclient.ui.main.adapter

import com.drakeet.multitype.MultiTypeAdapter
import com.example.githubclient.ui.model.UIRepositoryItem

class RepositoryAdapter : MultiTypeAdapter() {

    fun updateRepositoryStatus(repository: UIRepositoryItem, status: Int) {
        for (item in items) {
            if ((item as UIRepositoryItem).id == repository.id) {
                item.downloaded = status
                break
            }
        }
        notifyDataSetChanged()
    }
}