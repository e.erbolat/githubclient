package com.example.githubclient.ui.main

import android.Manifest
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubclient.R
import com.example.githubclient.common.Constants.STATUS_DOWNLOADED
import com.example.githubclient.common.Constants.STATUS_DOWNLOADING
import com.example.githubclient.common.presenter
import com.example.githubclient.ui.main.adapter.RepositoryAdapter
import com.example.githubclient.ui.main.viewBinder.RepositoryItemViewBinder
import com.example.githubclient.ui.model.UIRepositoryItem
import com.example.githubclient.ui.utils.showRequestPermissionToSettingsDialog
import kotlinx.android.synthetic.main.fragment_downloads.*
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_main.recyclerView
import kotlinx.android.synthetic.main.fragment_main.toolbar
import java.io.File


class MainFragment : Fragment(R.layout.fragment_main), MainContract.View {
    private val multiTypeAdapter = RepositoryAdapter()

    private val presenter: MainPresenter by presenter()

    private var repositoryItem: UIRepositoryItem? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity?)?.setSupportActionBar(toolbar)
        setupView()
        initAdapter()
    }

    private fun setupView() {
        downloadsIv.setOnClickListener {
            findNavController().navigate(R.id.action_main_fragment_to_downloads_fragment)
        }
        searchIv.setOnClickListener {
            if (searchEt.text.toString().isNotEmpty()) {
                presenter.searchRepositories(searchEt.text.toString())
            }
        }
        searchEt.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (searchEt.text.toString().isNotEmpty()) {
                    presenter.searchRepositories(searchEt.text.toString())
                }
            }
            return@setOnEditorActionListener false
        }
    }

    private fun initAdapter() {
        multiTypeAdapter.apply {
            register(RepositoryItemViewBinder(true, {
                repositoryItem = it
                requestStoragePermission()
            }, {
                openBrowserIntent(it)
            }))
        }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = multiTypeAdapter
        }
    }

    override fun setRepositories(items: List<UIRepositoryItem>) {
        multiTypeAdapter.items = items
        multiTypeAdapter.notifyDataSetChanged()
    }

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                downloadRepository()
            } else {
                activity?.showRequestPermissionToSettingsDialog(
                    context?.getString(R.string.storage_permission_go_to_settings).orEmpty()
                )
            }
        }

    private fun requestStoragePermission() {
        when {
            ContextCompat.checkSelfPermission(
                context ?: return,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED -> {
                downloadRepository()
            }
            shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
                requestPermissionLauncher.launch(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            }
            else -> {
                requestPermissionLauncher.launch(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            }
        }
    }

    override fun showLoader() {
        loaderFl.isVisible = true
    }

    override fun hideLoader() {
        loaderFl.isVisible = false
    }

    private fun openBrowserIntent(url: String) {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }

    private fun downloadRepository() {
        val myDir = File(Environment.DIRECTORY_DOWNLOADS + "/repositories")
        if (!myDir.exists()) {
            myDir.mkdirs()
        }

        val downloadManager =
            activity?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

        val downloadUri = Uri.parse(repositoryItem?.downloadUrl)
        val repositoryName = "${repositoryItem?.name}.zip"
        val request = DownloadManager.Request(downloadUri).apply {
            setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(getString(R.string.downloading_repository, repositoryName))
                .setDescription("")
                .setDestinationInExternalPublicDir(
                    myDir.toString(),
                    repositoryName
                )
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        }

        downloadManager.enqueue(request)
        multiTypeAdapter.updateRepositoryStatus(repositoryItem ?: return, STATUS_DOWNLOADING)
        Toast.makeText(
            context,
            getString(R.string.downloading_started, repositoryName),
            Toast.LENGTH_LONG
        ).show()
    }

    var onComplete: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(ctxt: Context, intent: Intent) {
            presenter.saveRepository(repositoryItem ?: return)
            multiTypeAdapter.updateRepositoryStatus(repositoryItem ?: return, STATUS_DOWNLOADED)
        }
    }

    override fun onResume() {
        super.onResume()
        context?.registerReceiver(
            onComplete,
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
    }
}