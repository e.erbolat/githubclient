package com.example.githubclient.ui.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import com.example.githubclient.R
import com.example.githubclient.common.Constants.STORAGE_PERMISSION

fun Activity.showRequestPermissionToSettingsDialog(
    titleText: String
) {
    val dialog = AlertDialog.Builder(this)
    dialog.setMessage(titleText)
    dialog.setPositiveButton(R.string.go_to_settings) { _, _ ->
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", this.packageName, null)
        intent.data = uri
        this.startActivityForResult(intent, STORAGE_PERMISSION)
    }
    dialog.setCancelable(false)
    dialog.setNegativeButton(R.string.not_now, null)
    dialog.create().show()
}