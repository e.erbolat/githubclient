package com.example.githubclient.ui.downloads

import com.example.githubclient.common.MvpPresenter
import com.example.githubclient.common.MvpView
import com.example.githubclient.ui.model.UIRepositoryItem

interface DownloadsContract {

    interface View : MvpView {
        fun setRepositories(items: List<UIRepositoryItem>)
    }

    interface Presenter : MvpPresenter<View> {
        fun getRepositories()
    }
}
