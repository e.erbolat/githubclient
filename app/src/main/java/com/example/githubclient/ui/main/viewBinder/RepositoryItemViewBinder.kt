package com.example.githubclient.ui.main.viewBinder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.drakeet.multitype.ItemViewBinder
import com.example.githubclient.R
import com.example.githubclient.common.Constants.STATUS_DEFAULT
import com.example.githubclient.common.Constants.STATUS_DOWNLOADED
import com.example.githubclient.common.Constants.STATUS_DOWNLOADING
import com.example.githubclient.ui.model.UIRepositoryItem
import kotlinx.android.synthetic.main.adapter_repository_item.view.*

class RepositoryItemViewBinder(
    val showButtons: Boolean,
    val downloadClickListener: (UIRepositoryItem) -> Unit,
    val browseClickListener: (String) -> Unit
) :
    ItemViewBinder<UIRepositoryItem, RepositoryItemViewBinder.ViewHolder>() {

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder {
        val view = inflater.inflate(R.layout.adapter_repository_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, item: UIRepositoryItem) {
        holder.bind(item)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(data: UIRepositoryItem) = with(itemView) {
            titleTv.text = data.name
            authorTv.text = context.getString(R.string.author, data.owner.name)
            browseIv.isVisible = showButtons
            downloadIv.isEnabled = data.downloaded == -1
            downloadIv.setOnClickListener {
                downloadClickListener.invoke(data)
            }
            if (showButtons) {
                when (data.downloaded) {
                    STATUS_DEFAULT -> {
                        downloadIv.alpha = 1f
                        downloadIv.visibility = View.VISIBLE
                        progressBar.isVisible = false
                    }
                    STATUS_DOWNLOADED -> {
                        downloadIv.alpha = 0.4f
                        downloadIv.visibility = View.VISIBLE
                        progressBar.isVisible = false
                    }
                    STATUS_DOWNLOADING -> {
                        downloadIv.visibility = View.INVISIBLE
                        progressBar.isVisible = true
                    }
                }
            } else {
                downloadIv.isVisible = false
            }

            browseIv.setOnClickListener {
                browseClickListener.invoke(data.url)
            }
        }
    }
}