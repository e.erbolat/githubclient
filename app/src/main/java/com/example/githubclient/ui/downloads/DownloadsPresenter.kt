package com.example.githubclient.ui.downloads

import com.example.githubclient.common.BasePresenter
import com.example.githubclient.domain.RepositoryInteractor
import kotlinx.coroutines.launch

class DownloadsPresenter(
    private val repositoryInteractor: RepositoryInteractor
) :
    BasePresenter<DownloadsContract.View>(),
    DownloadsContract.Presenter {

    private var prevAttachedView: DownloadsContract.View? = null
    private var isViewReattached = false
    override fun getRepositories() {
        launch {
            val listRepositories = repositoryInteractor.getRepositories()
            view?.setRepositories(listRepositories)
        }
    }

    override fun attach(view: DownloadsContract.View) {
        super.attach(view)
        isViewReattached = view == prevAttachedView
        prevAttachedView = view
    }

}