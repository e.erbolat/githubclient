package com.example.githubclient.network

import com.example.githubclient.network.model.SearchResultDTO
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Streaming
import retrofit2.http.Url

/**
 * Created by Yergali Zhakhan on 2020-04-07.
 */

interface ApiService {

    @GET("search/repositories")
    suspend fun searchRepository(
        @Query("q") query: String
    ): SearchResultDTO

    @GET
    @Streaming
    fun downloadRepository(@Url url: String?): Call<ResponseBody?>?
}