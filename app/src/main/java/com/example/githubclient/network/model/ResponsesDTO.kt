package com.example.githubclient.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchResultDTO(
    val items: List<Repository>
) : Parcelable

@Parcelize
data class Repository(
    val id: Int,
    val name: String,
    @SerializedName("html_url") val url: String,
    @SerializedName("url") val downloadUrl: String,
    @SerializedName("owner") val owner: Owner,
) : Parcelable

@Parcelize
data class Owner(
    val id: String,
    val login: String
) : Parcelable