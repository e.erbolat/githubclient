package com.example.githubclient.domain

import com.example.githubclient.common.Constants.STATUS_DEFAULT
import com.example.githubclient.common.Constants.STATUS_DOWNLOADED
import com.example.githubclient.data.model.Repository
import com.example.githubclient.ui.model.UIOwner
import com.example.githubclient.ui.model.UIRepositoryItem


class RepositoryInteractor(
    private val repositoryRemoteGateway: RepositoryRemoteGateway,
    private val repositoryLocalGateway: RepositoryLocalGateway
) {

    suspend fun searchRepository(query: String): List<UIRepositoryItem> {
        val response = repositoryRemoteGateway.searchRepository(query)
        val localRepositories = repositoryLocalGateway.provideRepositoryDao().getAll()
        val items = mutableListOf<UIRepositoryItem>()
        for (item in response.items) {
            val owner = UIOwner(item.owner.id, item.owner.login)
            var downloadUrl = item.downloadUrl + "/zipball"
            var downloaded = STATUS_DEFAULT
            for (repo in localRepositories) {
                if (item.id == repo.id) {
                    downloaded = STATUS_DOWNLOADED
                    break
                }
            }
            items += UIRepositoryItem(item.id, item.name, owner, item.url, downloadUrl, downloaded)
        }
        return items
    }

    suspend fun getRepositories(): List<UIRepositoryItem> {
        val response = repositoryLocalGateway.provideRepositoryDao().getAll()
        val items = mutableListOf<UIRepositoryItem>()
        for (item in response) {
            val owner = UIOwner(item.ownerId, item.ownerName)
            items += UIRepositoryItem(
                item.id,
                item.repositoryName,
                owner,
                item.url,
                "",
                STATUS_DOWNLOADED
            )
        }
        return items
    }

    suspend fun saveRepository(item: UIRepositoryItem) {
        val repository = Repository(item.id, item.name, item.owner.id, item.owner.name, item.url)
        repositoryLocalGateway.provideRepositoryDao().insertAll(repository)
    }
}