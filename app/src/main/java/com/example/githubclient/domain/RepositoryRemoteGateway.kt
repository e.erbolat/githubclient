package com.example.githubclient.domain

import com.example.githubclient.network.model.SearchResultDTO
import okhttp3.ResponseBody
import retrofit2.Call

interface RepositoryRemoteGateway {

    suspend fun searchRepository(query: String): SearchResultDTO

    suspend fun downloadRepository(url: String): Call<ResponseBody?>?
}