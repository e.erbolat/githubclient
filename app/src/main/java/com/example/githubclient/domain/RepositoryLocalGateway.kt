package com.example.githubclient.domain

import android.app.Application
import com.example.githubclient.data.AppDatabase
import com.example.githubclient.data.RepositoryDao

class RepositoryLocalGateway(application: Application) {
    private var appDatabase: AppDatabase = AppDatabase.getInstance(application)

    fun provideRepositoryDao(): RepositoryDao = appDatabase.repositoryDao()
}