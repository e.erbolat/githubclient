package com.example.githubclient.di

import com.example.githubclient.data.AppDatabase
import com.example.githubclient.data.RepositoryRestGateway
import com.example.githubclient.domain.RepositoryInteractor
import com.example.githubclient.domain.RepositoryLocalGateway
import com.example.githubclient.domain.RepositoryRemoteGateway
import com.example.githubclient.network.ApiService
import com.example.githubclient.ui.downloads.DownloadsPresenter
import com.example.githubclient.ui.main.MainPresenter
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

val dataModule = module {
    single { get<Retrofit>().create(ApiService::class.java) } bind ApiService::class
    single { AppDatabase.getInstance(androidContext()) } bind AppDatabase::class
    single { RepositoryRestGateway(get()) } bind RepositoryRemoteGateway::class
    single { RepositoryLocalGateway(androidApplication()) } bind RepositoryLocalGateway::class
}

val viewModelModule = module {
    viewModel { MainPresenter(get()) }
    viewModel { DownloadsPresenter(get()) }
}

val interactors = module {
    factory { RepositoryInteractor(get(), get()) }
}