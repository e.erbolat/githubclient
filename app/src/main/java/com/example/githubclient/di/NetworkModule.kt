package com.example.githubclient.di

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.example.githubclient.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit

object NetworkModule {

    private const val DEFAULT_CONNECT_TIMEOUT_SECONDS = 30L
    private const val DEFAULT_READ_TIMEOUT_SECONDS = 30L
    private const val DEFAULT_DISK_CACHE_SIZE = 256 * 1024 * 1024L

    fun createModule(): Module = module {

        single {
            val gson = get<Gson>()

            createOkHttpClient(get())
                .apply {
                    addLoggingInterceptor(gson)
                }
                .build()
        } bind OkHttpClient::class

        single {
            GsonBuilder()
                .apply { if (BuildConfig.DEBUG) setPrettyPrinting() }
                .create()
        } bind Gson::class

        single {
            Retrofit.Builder()
                .baseUrl(BuildConfig.HOST_URL)
                .addConverterFactory(GsonConverterFactory.create(get<Gson>()))
                .callFactory(get<OkHttpClient>())
                .build()
        } bind Retrofit::class

        single {
            CookieManager().apply { setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER) }
        } bind CookieManager::class

        single {
            PreferenceManager.getDefaultSharedPreferences(get())
        } bind SharedPreferences::class
    }

    private fun createOkHttpClient(
        context: Context
    ): OkHttpClient.Builder {

        return OkHttpClient.Builder()
            .readTimeout(DEFAULT_CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .connectTimeout(DEFAULT_READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .cache(
                Cache(
                    context.cacheDir,
                    DEFAULT_DISK_CACHE_SIZE
                )
            )
            .hostnameVerifier { _, _ -> true }
    }

    private fun OkHttpClient.Builder.addLoggingInterceptor(gson: Gson): OkHttpClient.Builder {

        val okHttpLogger = HttpLoggingInterceptor.Logger { }

        val interceptor = HttpLoggingInterceptor(okHttpLogger).apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        return addInterceptor(interceptor)
    }
}
