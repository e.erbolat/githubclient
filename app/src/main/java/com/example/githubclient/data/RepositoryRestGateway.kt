package com.example.githubclient.data

import com.example.githubclient.domain.RepositoryRemoteGateway
import com.example.githubclient.network.ApiService
import com.example.githubclient.network.model.SearchResultDTO
import okhttp3.ResponseBody
import retrofit2.Call

class RepositoryRestGateway(
    private val api: ApiService
) : RepositoryRemoteGateway {
    override suspend fun searchRepository(query: String): SearchResultDTO {
        return api.searchRepository(query)
    }

    override suspend fun downloadRepository(url: String): Call<ResponseBody?>? {
        return api.downloadRepository(url)
    }
}