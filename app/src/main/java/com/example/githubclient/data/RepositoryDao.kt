package com.example.githubclient.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.githubclient.data.model.Repository

@Dao
interface RepositoryDao {
    @Query("SELECT * FROM repository")
    suspend fun getAll(): List<Repository>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg repository: Repository)
}