package com.example.githubclient.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Repository(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "repository_name") val repositoryName: String,
    @ColumnInfo(name = "owner_id") val ownerId: String,
    @ColumnInfo(name = "owner_name") val ownerName: String,
    @ColumnInfo(name = "url") val url: String
)