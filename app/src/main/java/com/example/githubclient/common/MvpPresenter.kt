package com.example.githubclient.common

interface MvpPresenter<V : MvpView> {

    fun attach(view: V)

    fun detach()
}
