package com.example.githubclient.common

object Constants {
    const val STORAGE_PERMISSION = 100
    const val STATUS_DEFAULT = -1
    const val STATUS_DOWNLOADING = 1
    const val STATUS_DOWNLOADED = 2
}